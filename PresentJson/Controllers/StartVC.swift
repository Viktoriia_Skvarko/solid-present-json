//
//  StartVC.swift
//  PresentJson
//
//  Created by Viktoriia Skvarko on 04.08.2021.
//

import UIKit

class StartVC: UIViewController {
    
    // MARK: - IBOutlets

    @IBOutlet weak var textWelcome: UILabel!
    @IBOutlet weak var startButton: UIButton!
    
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        startButton.layer.cornerRadius = 5
    }
}
