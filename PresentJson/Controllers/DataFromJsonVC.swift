//
//  DataFromJson.swift
//  PresentJson
//
//  Created by Viktoriia Skvarko on 04.08.2021.
//

import UIKit

class DataFromJsonVC: UIViewController {
    
    // MARK: - Public Properties
    
    var dataFetcherService = DataFetcherService()
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var textApp: UILabel!
    
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presentData()
        
    }
    
    // MARK: - Business logic
    
    func presentData() {
        dataFetcherService.fetchData(completion: { (data) in
            let text = data?.feed.results.first?.copyright
            self.textApp.text = text
            
            let image = data?.feed.icon
            let urlImage = NSURL(string: image!)
            if let data = NSData(contentsOf: urlImage! as URL) {
                self.iconImage.image = UIImage(data: data as Data)
            }
        })
    }
    
}
