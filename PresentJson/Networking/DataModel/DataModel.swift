//
//  DataModel.swift
//  PresentJson
//
//  Created by Viktoriia Skvarko on 04.08.2021.
//

import Foundation

// Модель отображающая нужные данные из ссылки JSON пропущенной через валидатор JSON Formatter & Validator

struct  AppGroup: Decodable {
    let feed: Feed
}

struct Feed: Decodable {
    let icon: String
    let results: [FeedResult]
}

struct FeedResult: Decodable {
    let copyright: String
}

