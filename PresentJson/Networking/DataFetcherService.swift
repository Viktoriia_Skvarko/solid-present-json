//
//  DataFetcherService.swift
//  PresentJson
//
//  Created by Viktoriia Skvarko on 04.08.2021.
//

import Foundation

// Преобразуем данные хранящиеся по URL в нашу Модель данных DataModel

class DataFetcherService {
    
    var dataFetcher: DataFetcher
    
    init(dataFetcher: DataFetcher = NetworkDataFetcher()) {
        self.dataFetcher = dataFetcher
    }
    
    func fetchData(completion: @escaping (AppGroup?) -> Void) {
        let urlString = "https://rss.itunes.apple.com/api/v1/us/ios-apps/top-free/all/10/explicit.json"
        dataFetcher.fetchGenericJSONData(urlString: urlString, response: completion)
    }
}
